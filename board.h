#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <vector>


struct Point2D
{
    int x, y;
    Point2D(int _x, int _y) : x(_x), y(_y) {}
    Point2D() : Point2D(-1, -1) {}

    bool operator ==(Point2D other)
    {
        return x == other.x && y == other.y;
    }

    void operator +=(Point2D other)
    {
        x += other.x;
        y += other.y;
    }

    Point2D operator +(Point2D other)
    {
        return Point2D(x + other.x, y + other.y);
    }

    Point2D operator -(Point2D other)
    {
        return Point2D(x - other.x, y - other.y);
    }

    Point2D operator -()
    {
        return Point2D(-x, -y);
    }
};

class Board
{
    public:
        static const int BOARD_WIDTH  = 9;
        static const int BOARD_HEIGHT = 5;

        static const char PLAYER_PAWN = 'P';
        static const char ENEMY_PAWN  = 'E';
        static const char EMPTY_SPACE = '0';

        enum class Player
        {
            Me,
            Enemy
        };

        enum class SelectStatus
        {
            PawnSelected,
            PendingAction,
            NonExistingPawn,
            WrongPawn
        };

        enum class MoveStatus
        {
            PawnMovedNoTakes,
            PawnMovedTake,
            RequiresInteraction,
            UnableToMove
        };

        enum class TakeDirection
        {
            Forward,
            Backward
        };

    private:
        char m_board[BOARD_HEIGHT][BOARD_WIDTH];
        Player m_player = Player::Me;
        Point2D m_selectedPawn;
        
        bool m_requiresInteraction = false;
        bool m_combo = false;
        Point2D m_pendingDirection;
    
    protected:
        void togglePlayer();
        bool isMovePossible(Point2D& pawn, Point2D& destination);
        int takeInDirection(Point2D from, Point2D direction, bool simulate = false);

    public:
        Board();

        bool isMyTurn() const;
        bool isPawnSelected() const;
        bool isCombo() const;
        bool isTakingMove(Point2D& from, Point2D& to);
        bool nextMoveTakeExists();

        void deselectPawn();

        std::vector<Point2D> getPossibleSelections();
        std::vector<Point2D> getPossibleMoves();

        SelectStatus selectPawn(Point2D pawn);
        MoveStatus movePawn(Point2D space);
        void decideTakeDirection(TakeDirection direction);
        
        void drawBoard();
};


#endif