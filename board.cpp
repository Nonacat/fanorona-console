#include "board.h"


Board::Board()
{
    for(int i = 0; i < 2*BOARD_WIDTH; i++)
    {
        m_board[i / BOARD_WIDTH][i % BOARD_WIDTH] = ENEMY_PAWN;
        m_board[(3*BOARD_WIDTH + i) / BOARD_WIDTH][i % BOARD_WIDTH] = PLAYER_PAWN;
    }

    for(int i = 0; i < BOARD_WIDTH; i++)
    {
        m_board[2][i] = i % 2 == 0 ? ENEMY_PAWN : PLAYER_PAWN;
    }

    m_board[2][4] = EMPTY_SPACE;
}


bool Board::isMyTurn() const
{
    return m_player == Player::Me;
}


bool Board::isPawnSelected() const
{
    return not (m_selectedPawn.x < 0 || m_selectedPawn.y < 0);
}


bool Board::isCombo() const
{
    return m_combo;
}


bool Board::isTakingMove(Point2D& from, Point2D& to)
{
    Point2D direction = Point2D(
        to.x - from.x, to.y - from.y
    );
    int takesForward = takeInDirection(to, direction, true);
    int takesBackward = takeInDirection(to, -direction, true);

    return takesForward > 0 || takesBackward > 0;
}


bool Board::nextMoveTakeExists()
{
    auto moves = getPossibleMoves();
    for(auto& move : moves)
    {
        Point2D direction = Point2D(
            move.x - m_selectedPawn.x, move.y - m_selectedPawn.y
        );
        // Check if next move contains can take oponent's pawns
        int takesForward = takeInDirection(move, direction, true);
        int takesBackward = takeInDirection(move - direction, -direction, true);

        if(takesForward > 0 || takesBackward > 0)
            return true;
    }

    return false;
}


void Board::deselectPawn()
{
    m_selectedPawn = Point2D(-1, -1);
}


Board::SelectStatus Board::selectPawn(Point2D pawn)
{
    if(m_combo || m_requiresInteraction)
        return SelectStatus::PendingAction;

    // This switch handles wrong pawn selection
    switch(m_board[pawn.y][pawn.x])
    {
        case PLAYER_PAWN:
            if(not isMyTurn())
                return SelectStatus::WrongPawn;
        break;
        case ENEMY_PAWN:
            if(isMyTurn())
                return SelectStatus::WrongPawn;
        break;
        default:
            return SelectStatus::NonExistingPawn;
        break;
    }

    // If selected pawn is correct
    m_selectedPawn = Point2D(pawn.x, pawn.y);
    return SelectStatus::PawnSelected;
}


// Changes active the opposite one
void Board::togglePlayer()
{
    m_player = m_player == Player::Me ? Player::Enemy : Player::Me;
}


bool Board::isMovePossible(Point2D& pawn, Point2D& destination)
{
    // Ensure it's not outside of the board and the point is an empty space
    if(
        destination.x < 0 || destination.y < 0 ||
        destination.x >= BOARD_WIDTH || destination.y >= BOARD_HEIGHT ||
        m_board[destination.y][destination.x] != EMPTY_SPACE ||
        m_board[pawn.y][pawn.x] == EMPTY_SPACE || pawn == destination
    )
        return false;
    
    // One left or one right is correct
    if(abs(pawn.x - destination.x) == 1 && pawn.y - destination.y == 0)
        return true;
    
    // One up or one down is correct
    if(pawn.x - destination.x == 0 && abs(pawn.y - destination.y) == 1) 
        return true;
    
    // Only even board indexed pawns can move diagonally
    if((BOARD_WIDTH * pawn.y + m_selectedPawn.x) % 2 == 0)
        // True if the point is one move away diagonally
        return abs(pawn.x - destination.x) == 1 && abs(pawn.y - destination.y) == 1;
    else
        return false;
}


int Board::takeInDirection(Point2D from, Point2D direction, bool simulate)
{
    int pawnsTaken = 0;

    Point2D pawn = from + direction;
    char oponentPawn = isMyTurn() ? ENEMY_PAWN : PLAYER_PAWN;

    while(
        pawn.x >= 0 && pawn.x < BOARD_WIDTH &&
        pawn.y >= 0 && pawn.y < BOARD_HEIGHT &&
        m_board[pawn.y][pawn.x] == oponentPawn
    )
    {
        if(not simulate)
            m_board[pawn.y][pawn.x] = EMPTY_SPACE;

        pawn += direction;
        pawnsTaken += 1;
    }

    return pawnsTaken;
}


// Returns a list of all selections with non 0 possible moves
std::vector<Point2D> Board::getPossibleSelections()
{
    std::vector<Point2D> possibilities;

    for(int i = 0; i < BOARD_WIDTH * BOARD_HEIGHT; i++)
    {
        Point2D pawn = Point2D(i % BOARD_WIDTH, i / BOARD_WIDTH);

        // Ignore oponent's pawns
        if(m_board[pawn.y][pawn.x] == PLAYER_PAWN ^ isMyTurn())
            continue;

        for(int j = 0; j < BOARD_WIDTH * BOARD_HEIGHT; j++)
        {
            Point2D field = Point2D(j % BOARD_WIDTH, j / BOARD_WIDTH);
            if(isMovePossible(pawn, field))
                possibilities.push_back(pawn);
        }
    }

    return possibilities;
}


// Returns a list of all possible moves for selected pawn
std::vector<Point2D> Board::getPossibleMoves()
{
    std::vector<Point2D> possibilities;

    for(int i = 0; i < BOARD_WIDTH * BOARD_WIDTH; i++)
    {
        Point2D move = Point2D(i % BOARD_WIDTH, i / BOARD_WIDTH);
        const char pawn = m_board[move.y][move.x];
        
        if(isMovePossible(m_selectedPawn, move))
            possibilities.push_back(move);
    }

    return possibilities;
}


Board::MoveStatus Board::movePawn(Point2D space)
{
    if(isMovePossible(m_selectedPawn, space))
    {
        std::swap(
            m_board[space.y][space.x],
            m_board[m_selectedPawn.y][m_selectedPawn.x]
        );

        // Calculate direction in which the move was taken
        Point2D direction = Point2D(
            space.x - m_selectedPawn.x, space.y - m_selectedPawn.y
        );

        const char oponentPawn = isMyTurn() ? ENEMY_PAWN : PLAYER_PAWN;

        const int canTakeForward =
            m_board
                [space.y + direction.y]
                [space.x + direction.x]
            == oponentPawn;
            
        const int canTakeBackward =
            m_board
                [m_selectedPawn.y - direction.y]
                [m_selectedPawn.x - direction.x]
            == oponentPawn;
        
        m_selectedPawn.x = space.x;
        m_selectedPawn.y = space.y;

        // If player has to choose which oponent's pawns they want to take
        MoveStatus status;
        if(canTakeForward && canTakeBackward)
        {
            status = MoveStatus::RequiresInteraction;
            m_requiresInteraction = true;
            m_pendingDirection = direction;
        }
        // If there is only one way to take oponent's pawns or none
        else if(canTakeForward || canTakeBackward)
        {
            status = MoveStatus::PawnMovedTake;

            if(canTakeForward)
                takeInDirection(m_selectedPawn, direction);
            else
                takeInDirection(m_selectedPawn - direction, -direction);
            
            m_combo = nextMoveTakeExists();
        }
        else
        {
            status = MoveStatus::PawnMovedNoTakes;
            m_combo = false;
        }

        if(not m_combo && not m_requiresInteraction)
        {
            togglePlayer();
            deselectPawn();
        }

        return status;
    }
    else
        return MoveStatus::UnableToMove;
}


void Board::decideTakeDirection(TakeDirection direction)
{
    m_requiresInteraction = false;

    switch(direction)
    {
        case TakeDirection::Forward:
            takeInDirection(m_selectedPawn, m_pendingDirection);
        break;
        case TakeDirection::Backward:
            takeInDirection(
                m_selectedPawn - m_pendingDirection, -m_pendingDirection
            );
        break;
    }

    m_combo = nextMoveTakeExists();
    if(not isCombo())
    {
        togglePlayer();
        deselectPawn();
    }
}


void Board::drawBoard()
{
    // std::cout << "y\\x| ";
    // for(int i = 0; i < BOARD_WIDTH; i++)
    //     std::cout << i << "  ";
    // std::cout << std::endl;
    // std::cout << "---+---------------------------" << std::endl;

    // for(int i = 0; i < BOARD_HEIGHT; i++)
    // {
    //     std::cout << i << "  | ";
    //     for(int j = 0; j < BOARD_WIDTH; j++)
    //         std::cout << m_board[i][j] << "  ";
    //     std::cout << std::endl;
    // }

    std::cout << "y\\x| ";
    for(int i = 0; i < BOARD_WIDTH; i++)
        std::cout << i << " ";
    std::cout << std::endl;
    std::cout << "---+-------------------" << std::endl;

    const char* DIRECTIONS_EVEN = R"(|\|/|\|/|\|/|\|/|)";
    const char* DIRECTIONS_ODD  = R"(|/|\|/|\|/|\|/|\|)";

    for(int i = 0; i < BOARD_HEIGHT; i++)
    {
        std::cout << " " << i << " | ";
        for(int j = 0; j < BOARD_WIDTH; j++)
        {
            std::cout << m_board[i][j];
            if(j < BOARD_WIDTH - 1)
                std::cout << "-";
        }
        
        if(i < BOARD_HEIGHT - 1)
        {
            std::cout
                << std::endl
                << "   | "
                << (i % 2 == 0 ? DIRECTIONS_EVEN : DIRECTIONS_ODD)
                << std::endl
            ;
        }
    }

    std::cout << std::endl;
}