#include <iostream>
#include <cstdio>

#include "board.h"


const char* MENU = 
    "S - Select pawn to move\r\n"
    "M - Move selected pawn\r\n"
    "L - Display all pawns that have at least one legal move\r\n"
    "P - Display all possible moves for selected pawn\r\n";

const char* SELECT_PROMPT = "Select option: ";


int main()
{
    Board board;

    while(true)
    {
        // Draw board and options menu
        board.drawBoard();
        std::cout << std::endl;
        std::cout << MENU;

        char option;
        std::cout << SELECT_PROMPT;
        std::cin >> option;

        std::cout << std::endl;
        
        // Handle all options
        switch(option)
        {
            case 'S':
            {
                std::cout << "Provide position of the pawn" << std::endl;

                int x, y;
                std::cout << "x: ";
                std::cin >> x;
                std::cout << "y: ";
                std::cin >> y;

                Point2D selectedField = Point2D(x, y);
                auto selectionStatus = board.selectPawn(selectedField);

                switch(selectionStatus)
                {
                    case Board::SelectStatus::PawnSelected:
                        std::cout << "Pawn selected" << std::endl;
                    break;
                    case Board::SelectStatus::PendingAction:
                        std::cout << "Cannot move, you have to finish combo" << std::endl;
                    break;
                    case Board::SelectStatus::WrongPawn:
                        std::cout << "It's not your pawn!" << std::endl;
                    break;
                    case Board::SelectStatus::NonExistingPawn:
                        std::cout << "There is no spoo... I mean pawn" << std::endl;
                    break;
                }
            }
            break;
            case 'M':
            {
                std::cout << "Provide destination" << std::endl;

                int x, y;
                std::cout << "x: ";
                std::cin >> x;
                std::cout << "y: ";
                std::cin >> y;

                Point2D moveField = Point2D(x, y);
                auto movementStatus = board.movePawn(moveField);

                switch(movementStatus)
                {
                    case Board::MoveStatus::PawnMovedNoTakes:
                        std::cout << "Pawn was moved" << std::endl;
                    break;
                    case Board::MoveStatus::PawnMovedTake:
                        std::cout << "ENEMY DOWN!" << std::endl;
                    break;
                    case Board::MoveStatus::RequiresInteraction:
                    {
                        bool keepRunning = true;
                        while(keepRunning)
                        {
                            keepRunning = false;

                            std::cout << "You can take enemy's pawns forward or backward" << std::endl;
                            std::cout << "F - Take forward" << std::endl;
                            std::cout << "B - Take backward" << std::endl;
                            
                            char option;
                            std::cin >> option;

                            switch(option)
                            {
                                case 'F':
                                    board.decideTakeDirection(
                                        Board::TakeDirection::Forward
                                    );
                                break;
                                case 'B':
                                    board.decideTakeDirection(
                                        Board::TakeDirection::Backward
                                    );
                                break;
                                default:
                                    std::cout << "Wrong option" << std::endl;
                                    keepRunning = true;
                                break;
                            }
                        }
                    }
                    break;
                    case Board::MoveStatus::UnableToMove:
                        std::cout << "Space unreachable by selected pawn!" << std::endl;
                    break;
                }
            }
            break;
            case 'L':
            {
                auto pawns = board.getPossibleSelections();

                printf("You can move %lu pawns\r\n", pawns.size());
                for(auto& pawn : pawns)
                    printf("  x: %d, y: %d\r\n", pawn.x, pawn.y);
            }
            break;
            case 'P':
            {
                if(board.isPawnSelected())
                {
                    auto moves = board.getPossibleMoves();

                    printf("You have %lu possible moves\r\n", moves.size());
                    for(auto& move : moves)
                        printf("  x: %d, y: %d\r\n", move.x, move.y);
                }
                else
                    std::cout << "You have to select a pawn first!" << std::endl;
            }
            break;
            default:
                std::cout << "Whatcha' mean, bruh?" << std::endl;
            break;
        }
        std::cout << std::endl;

        // Oponent turn handle
        if(not board.isMyTurn())
        {
            board.drawBoard();
            std::cout << std::endl;

            Point2D pawn = board.getPossibleSelections().front();
            board.selectPawn(pawn);

            do
            {
                Point2D move = board.getPossibleMoves().front();
                if(board.movePawn(move) == Board::MoveStatus::RequiresInteraction)
                {
                    board.decideTakeDirection(Board::TakeDirection::Forward);
                }

                printf(
                    "Enemy moves (%d, %d) pawn to (%d, %d)\r\n",
                    pawn.x, pawn.y, move.x, move.y
                );
            }
            while(board.isCombo());
        }
    }
}